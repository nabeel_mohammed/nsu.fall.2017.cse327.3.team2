#The Software Design Specification Outline
*****************************************

#Introduction
*************
## 1.1 Purpose of this document

Our main aim was to make the software such a way so that the reader and the designer have enough and clear idea about our project. On this regard, this software design specification is designed.  This SDS in designed for those users, developers, project managers and testers who are in search of user friendly software. With a great overview this SDS will help the readers to have adequate idea about the ins and out of this project. 

## 1.2 Scope of the development project 

The main purpose of this software is to deliver product with a low cost from one place to another so that user can rely on. And also purchaser can purchase product through online and deliver it to the purchaser. Customer can send their product through this system faster rather than any other system and can get the product in a less time on the other hand the people who are carrying the product in their travel period they can earn money easily rather than wasting the carrying capacity . This system mainly save the money of a customer and also create a scope to earn money.

## 1.3 Definitions, acronyms, and abbreviations 

## 1.4 References

N/A 

## 1.5 Overview of document

This SDS is designed with the intro part having several sections including scopes, definitions, references and overview of this document as well. Reader will be able to have the idea of system architecture, design of component, user interfaces issue, execution architecture, design decision and tradeoffs.

#2 Architecture
****************
## UML Class Diagram

![UML Class Diagram](sds.png)

#3 Component Design Specification(s)
************************************
Our website is consist of many components. Each components are related and depended to each other in various ways. The overall descriptions of these components are given below:

## Website

In our class diagram Website is the main class. The whole class diagram is based on our website class. There is a class of web user on which our main class Website is depended. For any sort of transaction user will have to go to the website. Then they will be able to order for the product.  So this is why website main class is depended upon web user. The basic utilization of web user will make the website a good competitor on the market and make people rely on our system. 

## Webuser

The Webuser class holds the primary users of the website. As a result it has to subclasses, User and Admin. The main class Website is depended on Webuser in a relation of 1 to many. This is because Without Webuser, There will not be anyone to use or utilize of the main class Website itself.

## Admin

The Admin is a subclass of Webuser. It is because it inherits the class Webuser. It is related with the class Deliver in dependency. This is because Admin will make sure that the requested delivers by the customers are delivered accordingly. The relation between them is 1 to many. The Admin subclass is also depended on the class Order as Admin checks and make sure that the ordered products by the customers are delivered to the customers properly. The relation between them is 1 to many. The Contact class is depended on the subclass Admin because any users who tries to contact with the Admin will be replied by the Admin subclass itself. The relation between them is many to 1.

## user

Customer and carrier both are inherite the user class and registration class are dependent on user class on the other hand user class is also inherit the web user class .registration and user class have one to one relationship.

## Carrier

Carrier is a sub class of user class so it inherit the user class .product and contract both classes are dependent on the carrier class also and they have one to many relationship .

## Customer

Customer is a sub class of user class so it inherit the user class because user can have two type of user customer and carrier for that reason customer is a sub class of user class .order and delivery both class are dependent on customer class and they have one to one relationship .contract class is also dependent on customer class and they have one to many relationship.

## Security

Security is a class which is related with Admin subclass because Admin will maintain & check overall all kinds of security issues in every transaction. As a result the relation between them is 1 to many, Admin will check many security issues and as a result Admin subclass is depended on security class.

## Registration 

There is a class named registration in our class diagram. This registration class is depended on User class. This is an online registration procedure that means user will Register via online. They will have to register their identities. This will also ensure the valid user.  User will register by two ways. This is why there will be two sub classes from class Registration. One is Customer based Registration. Another one is Carrier based Registration. 

## Carrier based registration

This section is an important one because most of the job of delivering will have to be done by carrier in our system. Carrier will register via online and they will submit their basic identities. In addition, they will also have to submit the scan copy of national ID. As it is an international transaction, our system wants to make the environment user friendly and safety for any means of issue. 

## Customer based registration 

On the customer based registration customer will be the user. They will register on this section.  They will put their valid identities like name, address, age, mail id, country, postal code, gender etc here. For any sorts of transaction, they will be treated on basis of these identities.   

## Delivery

Deliver  class is dependent on customer and product class and admin class are depended on delivery class .deliver class and customer class have one to one relationship and delivery and product class have one to many relationship .delivery class and admin class have many to one relationship because can need to deliver multiple things .

## Order

Order class dependent on both customer and product class and admin class is dependent on order class. Order and customer class have one to one relationship. Order and product class have one to many relationship because order can be done for many product .order and admin have many to one relationship because multiple order can be done.

## Product

Pickup and drop class are dependent on the product class they both of them have one to many relationship on the other hand order, delivery and carrier class are dependent on the product class   and all of them have many to one relationship.

## Pickup

Pickup class dependent on product and it has one to many relationships with the product class 

## Drop

Drop class is also dependent on the product class and it has a one to much relationship with the product class.

## Contact

The Contact class is related with three subclasses. Carrier is one of the three subclasses. Contact is depended on the carrier subclass. The relation between them is many to 1 as one carrier can contact several times they want. Another Subclass is customer on which the class Contact is also depended. This is because one Customer can also contact several times. As a result, the relation between them is many to 1. Contact class is also depended on the Admin subclass in the relation of many to 1 because Admin will also able to contact as many times as they want. 




## 4 User Interface Issues
****************

This section will address User Interface issues as they apply to the following hypothetical users of our product -



1.Suppose a user is a 25 year old female who is fairly comfortable with technology.  She is proficient with using most

common computer applications. She wants to order something from our website. Since user  is comfortable with most common computer 

applications,the system will use common user interface conventions. She will be able to order easily through our web site. She will 

follow the instructions. She will first log in andthen look for dates to order a product when a carrier is going somewhere she wants.

Then, she will order and after ordering she will get a confirmation message of her order.

2.Suppose a user of 40 year old male who wants to be a carrier of our system. He will have to register first. For that he will have to 

sign up. Then,he will have to will register via online and they will submit their basic identities. In addition, 

he will also have to submit the scan copy of national ID. Then he can take the order and he will receive a confirmation message also.





As it is an international transaction, our system wants to make the environment user friendly and safety for any means of issue.



## 6 Design Decisions and Tradeoffs
********************

The design decision to break the system up into a three-module system was made to promote modularity within separate parts of the
system. Modularity provides encapsulation for the important pieces of the system. Using encapsulation, we are able to change 
important parts of the system without changing the whole system. For example, if the type of database were to be changed, 
only the DataAccess module would need to be altered. The specific database application program interface (API) is
encapsulated away into the DataAccess module. Therefore, the interface between modules can remain the same.
